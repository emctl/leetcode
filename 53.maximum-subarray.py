#
# @lc app=leetcode id=53 lang=python3
#
# [53] Maximum Subarray
#
from itertools import accumulate

# @lc code=start
class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        return max(accumulate(nums, lambda x, y: x+y if x > 0 else y))
# @lc code=end

