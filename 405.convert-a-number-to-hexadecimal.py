#
# @lc app=leetcode id=405 lang=python3
#
# [405] Convert a Number to Hexadecimal
#

# @lc code=start
class Solution:
    def toHex(self, num: int) -> str:
        if num<0:
            num+=2**32
        deque=collections.deque()
        s='0123456789abcdef'
        while num:
            deque.appendleft(s[num%16])
            num//=16
        return ''.join(deque) if deque else '0'
# @lc code=end

