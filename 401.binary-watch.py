#
# @lc app=leetcode id=401 lang=python3
#
# [401] Binary Watch
#

# @lc code=start
class Solution:
    def readBinaryWatch(self, turnedOn: int) -> List[str]:
        res = []
        for i in range(0, 12):
            for j in range(0, 60):
                if  (bin(i)+bin(j)).count('1') == turnedOn:
                    res.append('%d:%02d'%(i,j))
        return res         
# @lc code=end

