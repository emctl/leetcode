#
# @lc app=leetcode id=599 lang=python3
#
# [599] Minimum Index Sum of Two Lists
#

# @lc code=start
class Solution:
    def findRestaurant(self, list1: List[str], list2: List[str]) -> List[str]:

        rest2_to_index = {rest2 : index2 for index2, rest2 in enumerate(list2)}
        common_interest = {}

        for index1, rest1 in enumerate(list1):
            if rest1 in rest2_to_index:
                common_interest[rest1] = rest2_to_index[rest1] + index1

        least_index = min(common_interest.values())
        result = [rest for rest in common_interest if common_interest[rest] == least_index]

        return result   
# @lc code=end

