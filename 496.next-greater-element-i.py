#
# @lc app=leetcode id=496 lang=python3
#
# [496] Next Greater Element I
#

# @lc code=start
class Solution:
    def nextGreaterElement(self, nums1: List[int], nums2: List[int]) -> List[int]:
        stack = []
        table = {}
        for num in nums2:
            while stack and num > stack[-1]:
                table[stack.pop()] = num
            stack.append(num)
            
        return [ table.get(num, -1) for num in nums1]
# @lc code=end

