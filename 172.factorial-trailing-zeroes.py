#
# @lc app=leetcode id=172 lang=python3
#
# [172] Factorial Trailing Zeroes
#

# @lc code=start
class Solution:
    def trailingZeroes(self, n: int) -> int:
        k, total = 5, 0
        while k <= n:
            total += n // k
            k = k * 5
        return total
# @lc code=end

