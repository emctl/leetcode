#
# @lc app=leetcode id=338 lang=python3
#
# [338] Counting Bits
#

# @lc code=start
class Solution:
    def countBits(self, n: int) -> List[int]:
        setBits = [0] * (n+1)
        if n > 0:
            for i in range(1 ,n+1):
                setBits[i] = setBits[i & (i-1)] + 1
            return setBits  
        else:
            return [0]   
# @lc code=end

