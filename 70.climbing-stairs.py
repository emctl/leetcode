#
# @lc app=leetcode id=70 lang=python3
#
# [70] Climbing Stairs
#

# @lc code=start
class Solution:
    def climbStairs(self, n: int) -> int:
        if n == 0:
            return 0
        if n == 1:
            return 1
        if n == 2:
            return 2
        if 2 < n <= 45:
          a = b = 1
          for _ in range(n):
              a, b = b, a + b
          return a
# @lc code=end

