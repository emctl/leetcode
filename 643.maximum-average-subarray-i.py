#
# @lc app=leetcode id=643 lang=python3
#
# [643] Maximum Average Subarray I
#

# @lc code=start
class Solution:
    def findMaxAverage(self, nums: List[int], k: int) -> float:
        P = [0]
        for x in nums:
            P.append(P[-1] + x)

        ma = max(P[i+k] - P[i] 
                 for i in range(len(nums) - k + 1))
        return ma / float(k)       
# @lc code=end

