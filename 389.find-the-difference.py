#
# @lc app=leetcode id=389 lang=python3
#
# [389] Find the Difference
#

# @lc code=start
class Solution:
    def findTheDifference(self, s: str, t: str) -> str:
      res = ord(t[-1])
      for i in range(len(s)):
          res ^= ord(s[i]) ^ ord(t[i])
      return chr(res)
# @lc code=end

