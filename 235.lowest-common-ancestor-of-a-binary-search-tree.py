#
# @lc app=leetcode id=235 lang=python3
#
# [235] Lowest Common Ancestor of a Binary Search Tree
#

# @lc code=start
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
  def lowestCommonAncestor(self, root, p, q):
      while (root.val - p.val) * (root.val - q.val) > 0:
          root = (root.left, root.right)[p.val > root.val]
      return root        
# @lc code=end

