#
# @lc app=leetcode id=482 lang=python3
#
# [482] License Key Formatting
#

# @lc code=start
class Solution:
    def licenseKeyFormatting(self, s: str, k: int) -> str:
        S = s.replace('-', '')
        
        head = len(S) % k
        grouping = []
        
        if head:
            grouping.append( S[:head] )
        
        for index in range(head, len(S), k ):
            grouping.append( S[ index : index+k ] )
        
        
        return '-'.join( grouping ).upper()  
# @lc code=end

