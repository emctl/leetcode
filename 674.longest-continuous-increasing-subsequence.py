#
# @lc app=leetcode id=674 lang=python3
#
# [674] Longest Continuous Increasing Subsequence
#

# @lc code=start
class Solution:
    def findLengthOfLCIS(self, nums: List[int]) -> int:
        if not nums:
            return 0
        res = 1
        current = 1
        for i in range(1, len(nums)):
            current = current + 1 if nums[i] > nums[i-1] else 1
            res = max(res, current)
        return res
# @lc code=end

