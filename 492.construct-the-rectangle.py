#
# @lc app=leetcode id=492 lang=python3
#
# [492] Construct the Rectangle
#

# @lc code=start
import math

class Solution:
    def constructRectangle(self, area: int) -> List[int]:
      mid = int(math.sqrt(area))
      while area % mid != 0:
          mid -= 1
    
      return [int(area/mid),mid]
# @lc code=end

