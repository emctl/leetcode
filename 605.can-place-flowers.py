#
# @lc app=leetcode id=605 lang=python3
#
# [605] Can Place Flowers
#

# @lc code=start
class Solution:
    def canPlaceFlowers(self, flowerbed: List[int], n: int) -> bool:
        for i in range(len(flowerbed)):
            l = flowerbed[i - 1] if i > 0 else 0
            r = flowerbed[i + 1] if i < len(flowerbed) - 1 else 0
            if (l, flowerbed[i], r) == (0, 0, 0):
                n -= 1
                if n == 0:
                    return True
                flowerbed[i] = 1
        return n <= 0
# @lc code=end

