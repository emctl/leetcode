#
# @lc app=leetcode id=345 lang=python3
#
# [345] Reverse Vowels of a String
#

# @lc code=start
class Solution:
    def reverseVowels(self, s: str) -> str:
      vowels = re.findall('(?i)[aeiou]', s)
      return re.sub('(?i)[aeiou]', lambda m: vowels.pop(), s)
# @lc code=end

