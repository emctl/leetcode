#
# @lc app=leetcode id=541 lang=python3
#
# [541] Reverse String II
#

# @lc code=start
class Solution:
    def reverseStr(self, s: str, k: int) -> str:
        # Divide s into an array of substrings length k
        s = [s[i:i+k] for i in range(0, len(s), k)]
        # Reverse every other substring, beginning with s[0]
        for i in range(0, len(s), 2):
            s[i] = s[i][::-1]
        # Join array of substrings into one string and return 
        return ''.join(s)        
# @lc code=end

