#
# @lc app=leetcode id=108 lang=python3
#
# [108] Convert Sorted Array to Binary Search Tree
#

# @lc code=start
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def sortedArrayToBST(self, nums: List[int]) -> TreeNode:
        def dfs(left, right):
            if left > right: return None
            mid = (left + right) // 2
            return TreeNode(nums[mid], dfs(left, mid - 1), dfs(mid+1, right))

        
        return dfs(0, len(nums) - 1)
        
# @lc code=end

