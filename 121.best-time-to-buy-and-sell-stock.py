#
# @lc app=leetcode id=121 lang=python3
#
# [121] Best Time to Buy and Sell Stock
#

# @lc code=start
class Solution:
    def maxProfit(self, prices: List[int]) -> int:
      if len(prices) < 2:
        return 0

      max_profit = 0
      min_buy = prices[0]

      for p in prices[1:]:		
        max_profit = max(max_profit, p - min_buy)
        min_buy = min(min_buy, p)
      
      return max_profit
# @lc code=end

