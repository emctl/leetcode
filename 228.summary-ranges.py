#
# @lc app=leetcode id=228 lang=python3
#
# [228] Summary Ranges
#

# @lc code=start
class Solution:
    def summaryRanges(self, nums: List[int]) -> List[str]:
      ranges = []
      for n in nums:
          if not ranges or n > ranges[-1][-1] + 1:
              ranges += [],
          ranges[-1][1:] = n,
      return ['->'.join(map(str, r)) for r in ranges]
# @lc code=end

