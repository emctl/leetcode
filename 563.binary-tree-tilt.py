#
# @lc app=leetcode id=563 lang=python3
#
# [563] Binary Tree Tilt
#

# @lc code=start
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def findTilt(self, root: Optional[TreeNode]) -> int:
        self.tilt_sum = 0
        def DFS(root):
            if not root: return 0
            left_sum, right_sum = DFS(root.left), DFS(root.right)
            self.tilt_sum += abs(left_sum - right_sum)
            return left_sum + right_sum + root.val
        DFS(root)
        return self.tilt_sum  
# @lc code=end

